db.users.insertMany([
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21, 
		contact: {
			phone: "87654321",
			email: "janedoe@gmail.com"
		}, 
		courses: [ "CSS", "Javascript", "Python" ], 
		department: "HR"
	},
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76, 
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		}, 
		courses: [ "Python", "React", "PHP" ], 
		department: "HR"
	},
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
	{
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active"
    }
])


// comparison query operators
/*
	
*/

// %gt /  %gte operator
/*
	allow us to have documents that have field numebrs values greater than or equal to specified value

	syntax
		db.collectionName.find({field:{$gt:value}});
		db.collectionName.find({field:{$gte:value}});
*/
// 66 and above
db.users.find({age:{$gt:65}});
// 65 and above
db.users.find({age:{$gte:65}});


// $lt / $lte operator
/*
	allow us to have documents that have field numebrs values less than or equal to specified value
	
	syntax
	db.collectionName.find({field:{$lt:value}});
	db.collectionName.find({field:{$lte:value}});

*/

db.users.find({age:{$lt:76}});
db.users.find({age:{$lte:76}});


// $ne operator
/*
	allow us to have documents that have field numbers values not equal to specified value
	
	syntax
	db.collectionName.find({field:{$ne:value}});
	

*/

db.users.find({age:{$ne:82}});


//logical query operators

//$or
/*
	allow us to have documents that match a single criteria from multiple search criteria provided

	syntax
	db.collectionName.find({$or:[fieldA:valueA},{fieldB:valueB}]);	
*/

db.users.find({$or:[{firstName:"Neil"},{firstName:"Bill"}]});

db.users.find({$or:[{firstName:"Neil"},{firstName:"Bill"},{firstName:"Stephen"}]});

db.users.find({$or: [{firstName:"Neil"}, {age: {$gt:30}}]});


// $and
/*

	allow us to have documents that match a  criteria in a single field

	syntax
	db.collectionName.find({and:[fieldA:valueA},{fieldB:valueB}]);	

*/

db.users.find({$and: [
	{age:{$ne:82}},
	 {age: {$ne:76}}
	 ]});


//no output
// db.users.find({$and: [
// 	{age:{$lt:65}},
// 	 {age: {$gt:65}}
// 	 ]});


//field projection
/*
	when we retrieve document mongo db usually returens the whole document

	there are times when we only need specific fields 

	for those case we can includeor exclude fields from the response
*/


//inclusion
/*
	allowss us to inclide/ add specificc only when retriving documents 

	write 1 to include field

	syntax
	 	db.users.find({criteria}, {field:1})
*/

db.users.find(
		{
			firstName: "Jane"
		}
		,
		{
			firstName: 1,
			lastName:1,
			"contact.phone":1
		}

	)

//exclusion
/*
	allow us to exclude /remive specific field when displaying documents

	the value provided is "0" to deonte the field is being excluded

	syntax
	db.users.find({criteria}, {field:0})

*/

//exclude contact and department for jane
db.users.find(
		{
			firstName: "Jane"
		}
		,
		{
			contact: 0,
			department:0
			
		}

	)

//supressing the ID field

/*
	allows us to exclude the "_id" field when retrieving documents. 

	when using field projection, field inclusion and exclusion may not be usd at the same time 

	trhe "_id" is exempted to this rule

	syntax
		db.users.find({cruteria}, {id:0}) 
*/
db.users.find(
		{
			firstName: "Jane"
		}
		,
		{
			firstName:1,
			lastName:1,
			contact: 1,
			_id:0
		}

	)

//evaluation query operators

//$regex operator
/*
	allows us to find documetns that match a specific string pattern using regular expression

	syntax
	db.users.find({field: $regex: 'pattern', $options: '$optionValue'})
*/
//case sensitive query
db.users.find({firstName:{$regex: 'N'}})

//case Insensitive query
db.users.find({firstName:{$regex: 'N', $options:'$i'} })







